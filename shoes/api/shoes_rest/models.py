from django.db import models

# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, null=True, blank=True, unique=True)
    name = models.CharField(max_length=200)



class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    shoe_color = models.CharField
    picture_url = models.URLField(max_length=200, null=True, blank=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete= models.CASCADE
    )

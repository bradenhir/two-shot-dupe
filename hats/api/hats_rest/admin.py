from django.contrib import admin
from .models import LocationVO, Hats
# Register your models here.


@admin.register(Hats)
class HatsAdmin(admin.ModelAdmin):
    pass


@admin.register(LocationVO)
class LocationVoAdmin(admin.ModelAdmin):
    pass

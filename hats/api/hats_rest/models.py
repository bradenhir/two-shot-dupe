from django.db import models

# Create your models here.


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, null=True, unique=True, blank=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveIntegerField(default=None)
    shelf_number = models.PositiveIntegerField(default=None)

    def __str__(self):
        return f"closet name: {self.closet_name}, section number: {self.section_number}, shelf number: {self.shelf_number}"


class Hats(models.Model):
    name = models.CharField(max_length=200, default=None)
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=200)
    hat_color = models.CharField(max_length=200)
    hat_url = models.URLField(max_length=200, null=True, blank=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,

    )

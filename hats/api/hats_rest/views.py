from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import LocationVO, Hats
# Create your views here.


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'import_href',
        'closet_name',
        'section_number',
        'shelf_number',

    ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "name",
        "fabric",
        "style_name",
        "hat_color",
        'hat_url',
        'id',
        'location',

    ]
    encoders = {
        'location': LocationVOEncoder()
    }


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "name",
        "fabric",
        "style_name",
        "hat_color",
        'hat_url',
        'location',

    ]
    encoders = {
        "location": LocationVOEncoder()
    }


@require_http_methods(["GET", "POST"])
def hats_list(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {
                "hats": hats
            },
            encoder=HatsListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            href_loc = content['location']
            location = LocationVO.objects.get(import_href=href_loc)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400
                )

        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def hats_details(request, id):
    if request.method == "GET":
        hat = Hats.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False
        )
    else:
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

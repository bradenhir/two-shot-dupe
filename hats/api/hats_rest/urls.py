from .views import hats_list, hats_details
from django.urls import path

urlpatterns = [
    path("hats/", hats_list, name="hats_list"),
    path("hats/<int:id>/", hats_details, name='hats_details')
]

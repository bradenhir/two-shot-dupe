# Generated by Django 4.0.3 on 2023-07-22 03:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0005_locationvo_section_number_locationvo_shelf_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='hats',
            name='name',
            field=models.CharField(default=None, max_length=200),
        ),
    ]
